#!/usr/bin/env python

import os
import subprocess
import time
import platform
import string
import sys
import random

# Define Operating System Family
oper = str(platform.dist()[0])
try:
    import configparser
except ImportError:
    print("### Config Parser not detected, installing... ###")
    if oper == 'Ubuntu':
        subprocess.call("apt-get install -y python-configparser", shell=True)
    elif oper == 'centos':
        subprocess.call("yum install -y python-configparser", shell=True)
    else:
        print("Sorry, your Operating System is not supported")
        exit(1)
    import configparser

config = configparser.ConfigParser()

def getInfo():
    if os.path.isfile("/proc/user_beancounters"):
        print("\n### Sorry, this is an OpenVZ/Virtuozzo container and docker is not supported.  Please try a different provider. ###")
        print("### Vultr - https://www.vultr.com/?ref=7944315-4F is an excellent alternative ###\n\n")
        exit(0)
    coin = raw_input("What is the index number for the Masternode would you like to setup?\n")
    if int(coin) == 1:
        f = open("/root/masternode/coins/MNs.cfg", "w")
        f.write("\n[XRD]")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ndocker = calebcall/ravendark-docker")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nrpcport = 7207")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nport = 6666")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nrpcuser = ravendarkuser")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ndatadir = /root/.ravendarkcore_1")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nhostdir = /root/.ravendarkcore_1")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nconfig_file = ravendark.conf")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ncli = ravendark-cli")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ncoind = ravendarkd")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nticker = XRD")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nsentinel = true")
        f.close()               
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nsentinel_url = https://github.com/raven-dark/sentinel.git")
        f.close()
    elif int(coin) == 2:
        f = open("/root/masternode/coins/MNs.cfg", "w")
        f.write("\n[XRD]")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ndocker = calebcall/ravendark-docker")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nrpcport = 7207")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nport = 6666")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nrpcuser = ravendarkuser")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ndatadir = /root/.ravendarkcore_2")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nhostdir = /root/.ravendarkcore_2")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nconfig_file = ravendark.conf")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ncli = ravendark-cli")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ncoind = ravendarkd")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nticker = XRD")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nsentinel = true")
        f.close()               
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nsentinel_url = https://github.com/raven-dark/sentinel.git")
        f.close()             
    elif int(coin) == 3:
        f = open("/root/masternode/coins/MNs.cfg", "w")
        f.write("\n[XRD]")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ndocker = calebcall/ravendark-docker")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nrpcport = 7207")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nport = 6666")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nrpcuser = ravendarkuser")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ndatadir = /root/.ravendarkcore_3")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nhostdir = /root/.ravendarkcore_3")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nconfig_file = ravendark.conf")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ncli = ravendark-cli")
        f.close()
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\ncoind = ravendarkd")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nticker = XRD")
        f.close()        
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nsentinel = true")
        f.close()               
        f = open("/root/masternode/coins/MNs.cfg", "a")
        f.write("\nsentinel_url = https://github.com/raven-dark/sentinel.git")
        f.close()             
    else:
        print("The index number must be lower than 4.\n")
        time.sleep(1)
        getInfo()
    externalip = raw_input("Input the new ipv4 address.\n")
    f = open("/root/masternode/coins/MNs.cfg", "a")
    f.write("\nexternalip = %s" % (externalip))
    f.close()
    mn = "XRD"
    print("Installing %s" % (externalip))
    time.sleep(1)
    config.read('coins/MNs.cfg')
    install(mn)

def install(mn):
    if os.path.isfile("%s/%s" % (config[mn]['hostdir'],config[mn]['config_file'])):
        upgrade = raw_input("Do you want to upgrade %s? (y/n): " % (config[mn]['coind']))
        if "y" in upgrade:
            subprocess.call("docker pull %s" % (config[mn]['docker']), shell=True)
            subprocess.call("docker stop %s" % (config[mn]['coind']), shell=True)
            subprocess.call("docker rm %s" % (config[mn]['coind']), shell=True)
            if os.path.isfile("%s/%s-sentinel/sentinel.conf" % (config[mn]['hostdir'], config[mn]['ticker'].lower())):
                print("### Pulling latest sentinel ###")
                subprocess.call("cd %s/%s-sentinel/ && git pull" % (config[mn]['hostdir'], config[mn]['ticker'].lower()), shell=True)
            elif config[mn]['sentinel'] == 'true':
                if oper == 'Ubuntu':
                    subprocess.call("apt-get install -y python-crontab virtualenv", shell=True)
                elif oper == 'centos':
                    subprocess.call("pip install python-crontab virtualenv", shell=True)
                else:
                    exit(1)
                from crontab import CronTab
                subprocess.call("cd %s && git clone %s %s-sentinel && mkdir %s-sentinel/database" % (config[mn]['hostdir'], config[mn]['sentinel_url'], config[mn]['ticker'].lower(), config[mn]['ticker'].lower()), shell=True)
                subprocess.call("cd %s/%s-sentinel && virtualenv ./venv && ./venv/bin/pip install -r requirements.txt" % (config[mn]['hostdir'], config[mn]['ticker'].lower()), shell=True)
                cron = CronTab(user=True)
                cron.new(command="cd %s/%s-sentinel && ./venv/bin/python bin/sentinel.py >/dev/null 2>&1" % (config[mn]['hostdir'], config[mn]['ticker'].lower()))
                cron.write()
            subprocess.call("docker run -p %s:%s -p %s:%s -v %s:%s -d --network host --restart always --name %s %s" % (config[mn]['port'], config[mn]['port'], config[mn]['rpcport'], config[mn]['rpcport'], config[mn]['hostdir'], config[mn]['datadir'], config[mn]['coind'], config[mn]['docker']), shell=True)
            time.sleep(2)
            print("Wallet has been upgraded.")
            exit(0)
        elif "n" in upgrade:
            reinstall = raw_input("Do you want to reinstall %s? (y/n): " % (config[mn]['coind'])).lower()
            if "y" in reinstall:
                installDocker(mn)
            else:
                exit(1)
        else:
            exit(1)
    else:
        if oper == 'Ubuntu':
            subprocess.call("apt-get install -y python-pip dnsutils", shell=True)
        elif oper == 'centos':
            subprocess.call("yum install -y python-pip bind-utils", shell=True)
        else:
            print("OS is not currently supported")
            exit(1)
        subprocess.call("pip install configparser", shell=True)
        installDocker(mn)


def installDocker(mn):
    if oper == 'Ubuntu':
        subprocess.call("apt-get remove -y docker docker-engine docker.io containerd runc", shell=True)
        subprocess.call("apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common", shell=True)
        subprocess.call("curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -", shell=True)
        subprocess.call("add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"", shell=True)
        subprocess.call("apt-get update", shell=True)
        subprocess.call("apt-get install -y docker-ce", shell=True)
        subprocess.call("systemctl start docker", shell=True)
        subprocess.call("systemctl enable docker", shell=True)
    elif oper == 'centos':
        subprocess.call("yum remove -y docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine", shell=True)
        subprocess.call("yum install -y yum-utils device-mapper-persistent-data lvm2", shell=True)
        subprocess.call("yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo", shell=True)
        subprocess.call("yum install -y docker-ce", shell=True)
        subprocess.call("systemctl start docker", shell=True)
        subprocess.call("systemctl enable docker", shell=True)
    else:
        print("Sorry, your OS is not currently supported")
        exit(1)
    installDaemon(mn)


def installDaemon(mn):
    pwd = pwd_generator()
    rc = subprocess.call("docker ps -a | grep %s" % (config[mn]['coind']), shell=True)
    if rc == 0:
        subprocess.call("docker stop %s" % (config[mn]['coind']), shell=True)
        subprocess.call("docker rm %s" % (config[mn]['coind']), shell=True)
    print("\n### Pulling Docker Image ###\n")
    subprocess.call("docker pull %s" % (config[mn]['docker']), shell=True)
    subprocess.call("mkdir -p %s" % (config[mn]['hostdir']), shell=True)
    print("\n### Creating config ###\n")
    f = open("%s/%s" % (config[mn]['hostdir'],config[mn]['config_file']), "w")
    f.write("rpcuser=%s\nrpcpassword=%s\nrpcport=%s\nrpcallowip=127.0.0.1\nport=%s\nexternalip=%s\nlisten=1\nmaxconnections=256\n" % (config[mn]['rpcuser'], pwd, config[mn]['rpcport'], config[mn]['port'], config[mn]['externalip']))
    f.close()
    print("\n### Starting %s daemon ###\n" % (mn))
    subprocess.call("docker run -p %s:%s -p %s:%s -v %s:%s -d --network host --restart always --name %s %s" % (config[mn]['port'], config[mn]['port'], config[mn]['rpcport'], config[mn]['rpcport'], config[mn]['hostdir'], config[mn]['datadir'], config[mn]['coind'], config[mn]['docker']), shell=True)
    f = open("/usr/local/bin/%s" % (config[mn]['coind']), "w")
    f.write("docker exec -it %s %s" % (config[mn]['coind'],config[mn]['coind']))
    f.close()
    f = open("/usr/local/bin/%s" % (config[mn]['cli']), "w")
    f.write("docker exec -it %s %s $1 $2 $3 $4 $5" % (config[mn]['coind'], config[mn]['cli']))
    f.close()
    subprocess.call("chmod +x /usr/local/bin/*", shell=True)
    configMn(mn, pwd)


def configMn(mn, pwd):
    print("\n### Generating Masternode Private key ###\n")
    while True:
        try:
            key = subprocess.check_output("%s masternode genkey" % (config[mn]['cli']), shell=True).rstrip()
        except:
            continue
        else:
            break
    print("\n### Writing Masternode config parameters ###\n")
    subprocess.call("docker stop %s" % (config[mn]['coind']), shell=True)
    f = open("%s/%s" % (config[mn]['hostdir'],config[mn]['config_file']), "a")
    f.write("masternode=1\nmasternodeprivkey=%s\n" % (key))
    f.close()
    subprocess.call("docker start %s" % (config[mn]['coind']), shell=True)
    installSentinel(mn, pwd, key)


def installSentinel(mn, pwd, key):
    if config[mn]['sentinel'] == 'true':
        print("\n### Installing Sentinel ###\n")
        if oper == 'Ubuntu':
            subprocess.call("apt-get install -y python-crontab virtualenv", shell=True)
        elif oper == 'centos':
            subprocess.call("pip install python-crontab virtualenv", shell=True)
        else:
            exit(1)
        from crontab import CronTab
        subprocess.call("cd %s && git clone %s %s-sentinel && mkdir %s-sentinel/database" % (config[mn]['hostdir'], config[mn]['sentinel_url'], config[mn]['ticker'].lower(), config[mn]['ticker'].lower()), shell=True)
        # sentinelConfig = "%s/%s-sentinel/sentinel.conf" % (config[mn]['hostdir'], config[mn]['ticker'].lower())
        # if mn is 'XRD':
        #     with open(sentinelConfig, "r") as f:
        #         replaceText = f.read().replace('#ravendark_conf=/home/<username>/.ravendarkcore/ravendark.conf', 'ravendark_conf=/root/ravendarkcore/ravendark.conf')
        #     with open(sentinelConfig, "w") as f:
        #         f.write(replaceText)
        # else:
        #     with open(sentinelConfig, "r") as f:
        #         replaceText = f.read().replace(config[mn]['datadir'], config[mn]['hostdir'])
        #     with open(sentinelConfig, "w") as f:
        #         f.write(replaceText)
        subprocess.call("cd %s/%s-sentinel && virtualenv ./venv && ./venv/bin/pip install -r requirements.txt" % (config[mn]['hostdir'], config[mn]['ticker'].lower()), shell=True)
        cron = CronTab(user=True)
        cron.new(command="cd %s/%s-sentinel && ./venv/bin/python bin/sentinel.py >/dev/null 2>&1" % (config[mn]['hostdir'], config[mn]['ticker'].lower()))
        cron.write()
    showResults(mn, pwd, key)


def showResults(mn, pwd, key):
    print("\n============================= Node Information ============================================\n")
    print("rpcuser: %s" % (config[mn]['rpcuser']))
    print("rpcpassword: %s" % (pwd))
    print("Masternode IP: %s:%s" % (config[mn]['externalip'], config[mn]['port']))
    print("Masternode private key: %s" % (key))
    print("Config file: %s/%s" % (config[mn]['hostdir'], config[mn]['config_file']))
    print("\n\033[96mCopy the following for your local masternode.conf:\033[00m \033[91m%s:%s %s\033[00m" % (config[mn]['externalip'], config[mn]['port'], key))
    print("\n\n=========================================================================================\n")
    print("Check your node status with: %s masternode status" % (config[mn]['cli']))
    print("Stop your node with: docker stop %s" % (config[mn]['coind']))
    print("Start your node with: docker start %s" % (config[mn]['coind']))
    print("\n=========================================================================================\n")


def pwd_generator(size=32, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

if __name__ == '__main__':
    getInfo()
